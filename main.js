function onLoad() {
    console.log("js loaded")
}

window.onload = function(){
    console.log(document.getElementById("inputContainer").querySelectorAll(".inputField").length); 
}

function addField() {
    if(document.getElementById("inputContainer").querySelectorAll(".inputField").length < 30) {
        var input = document.createElement("input");
        input.type = "text";
        input.className = "inputField"; // set the CSS class
        input.maxLength = "100";
        input.placeholder = "Type something";
        document.getElementById("inputContainer").appendChild(input); // put it into the DOM
    } else {
        console.log("Max amount of fields reached.");
    }
}

function removeLastElement(divID) {
    try {
        var select = document.getElementById(divID);
        select.removeChild(select.lastChild);
    }
    catch(err) {
        console.log("Couldn't remove last row. (Is there any?)")
    }
}

function clearDiv(divID) {
    document.getElementById(divID).innerHTML = "";
}

function clearField(fieldID) {
    document.getElementById(fieldID).value = "";
}

function submitFields() {
    var fields = document.getElementById('inputContainer').getElementsByTagName('input');
    var words = [];
    for(i = 0; i < fields.length; i++) {
        var fieldValue = fields[i].value;
        
        if(fieldValue != "") {
            //add to array and strip spaces
            words.push(fieldValue.replace(/\s/g,''));
        }
    }
    words = shuffle(words);
    document.getElementById("outputField").value = words.join(" ");
}

// Fisher-Yates Shuffle
function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}